const fs = require("fs");
const path = require("path");

const { validationResult } = require("express-validator");

const socket = require("../socket");
const Post = require("../models/post");

exports.getPosts = async (req, res, next) => {
  try {
    const currentPage = req.query.page || 1;
    const totalItems = await Post.countDocuments();
    const posts = await Post.find()
      .populate("creator")
      .sort({ createdAt: -1 })
      .skip((currentPage - 1) * 2)
      .limit(2);

    res.status(200).send({
      posts,
      totalItems
    });
  } catch (e) {
    next(e);
  }
};

exports.getPost = async (req, res, next) => {
  try {
    const post = await Post.findById(req.params.id);
    if (!post) {
      const error = new Error("No post found for this id");
      error.status = 404;
      throw error;
    }

    res.status(200).send({
      post
    });
  } catch (e) {
    next(e);
  }
};

exports.createPost = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = new Error("Validation failed, please enter valid data");
      error.status = 422;
      throw error;
    }

    if (!req.file) {
      const error = new Error("No image provided");
      error.status = 422;
      throw error;
    }

    const post = await new Post({
      ...req.body,
      imageUrl: "images/" + req.file.filename,
      creator: req.user._id
    }).save();

    const user = req.user;
    user.posts.push(post);
    await user.save();

    socket.getIo().emit("posts", { action: "created", post });

    res.status(201).send({
      message: "Post saved !",
      post
    });
  } catch (e) {
    next(e);
  }
};

exports.updatePost = async (req, res, next) => {
  try {
    const post = await Post.findOne({
      _id: req.params.id,
      creator: req.user._id
    }).populate("creator");
    if (!post) {
      const error = new Error("Post not found");
      error.status = 404;
      throw error;
    }

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = new Error("Validation failed, please enter valid data");
      error.status = 422;
      throw error;
    }

    const imageUrl = req.file ? "images/" + req.file.filename : req.body.image;
    if (!imageUrl) {
      const error = new Error("No image provided");
      error.status = 422;
      throw error;
    }

    if (imageUrl !== post.imageUrl) {
      clearImage(post.imageUrl);
    }

    post.title = req.body.title;
    post.content = req.body.content;
    post.imageUrl = imageUrl;
    await post.save();

    socket.getIo().emit("posts", { action: "updated", post });

    res.status(200).send({ message: "Post updated", post });
  } catch (e) {
    next(e);
  }
};

exports.deletePost = async (req, res, next) => {
  try {
    const post = await Post.findOneAndDelete({
      _id: req.params.id,
      creator: req.user._id
    });
    if (!post) {
      const error = new Error("Post not found");
      error.status = 404;
      throw error;
    }

    clearImage(post.imageUrl);
    const user = req.user;
    user.posts.pull(req.params.id);
    await user.save();

    socket.getIo().emit("posts", { action: "deleted", post });

    res.status(200).send({ message: "Post deleted" });
  } catch (e) {
    next(e);
  }
};

const clearImage = filePath => {
  filePath = path.join(__dirname, "../../public/", filePath);
  fs.unlink(filePath, err => console.log(err));
};
