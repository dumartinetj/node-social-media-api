const { validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const config = require("../config");
const User = require("../models/user");

exports.registration = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = new Error("Validation error");
      error.status = 422;
      error.data = errors.array();
      throw error;
    }

    const password = await bcrypt.hash(req.body.password, 12);

    const user = await new User({
      ...req.body,
      password
    }).save();

    res.status(201).send({ message: "User created", user });
  } catch (e) {
    next(e);
  }
};

exports.login = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = new Error("Validation error");
      error.status = 422;
      error.data = errors.array();
      throw error;
    }

    const user = await User.findOne({ email: req.body.email });
    if (!user) {
      const error = new Error("Invalid credentials");
      error.status = 401;
      throw error;
    }

    const passwordMatch = await bcrypt.compare(
      req.body.password,
      user.password
    );
    if (!passwordMatch) {
      const error = new Error("Invalid credentials");
      error.status = 401;
      throw error;
    }
    const token = jwt.sign(
      {
        _id: user._id.toString(),
        email: user.email,
        name: user.name
      },
      process.env.JWT_SECRET,
      { expiresIn: "10h" }
    );

    res
      .status(200)
      .send({
        message: "Login successful",
        userId: user._id.toString(),
        token
      });
  } catch (e) {
    next(e);
  }
};
