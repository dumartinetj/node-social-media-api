const path = require("path");

const config = require("./config");
const express = require("express");
const mongoose = require("mongoose");
const multer = require("multer");

const socket = require("./socket");
const authRoutes = require("./routes/auth");
const feedRoutes = require("./routes/feed");

const app = express();

// JSON Request
app.use(express.json());

// Expose static public folder
app.use(express.static(path.join(__dirname, "../public")));

// Multer config for file uploads
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/images");
  },
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString() + "-" + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.use(multer({ storage: fileStorage, fileFilter }).single("image"));

// CORS
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

// Routes
app.use("/auth", authRoutes);
app.use("/feed", feedRoutes);

// Global error handler
app.use((error, req, res, next) => {
  res
    .status(error.status || 500)
    .send({ message: error.message, data: error.data });
});

mongoose
  .connect(config.databaseURL)
  .then(() => {
    const server = app.listen(config.port);
    const io = socket.init(server);
    io.on("connection", socket => {
      console.log("Socket connected");
    });
  })
  .catch(e => {
    console.log("Error while connecting to MongoDB Atlas");
  });
