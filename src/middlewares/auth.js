const jwt = require("jsonwebtoken");

const config = require("../config");

const User = require("../models/user");

module.exports = async (req, res, next) => {
  try {
    const token = req.get("Authorization");
    if (!token) {
      const error = new Error(
        "Unauthorized, please provide an authentication token"
      );
      error.status = 401;
      throw error;
    }
    const decodedToken = await jwt.verify(
      token.split(" ")[1],
      config.jwtSecret
    );
    if (!decodedToken) {
      const error = new Error("Unauthorized, please authenticate");
      error.status = 401;
      throw error;
    }
    const user = await User.findById(decodedToken._id);
    if (!user) {
      const error = new Error("Unauthorized, user doesn't exist");
      error.status = 401;
      throw error;
    }
    req.user = user;
    next();
  } catch (e) {
    next(e);
  }
};
