const dotenv = require("dotenv");

const envFound = dotenv.config();
if (!envFound) {
  throw new Error("No .env file found");
}

module.exports = {
  port: process.env.PORT,
  databaseURL: process.env.DATABASE_URL,
  jwtSecret: process.env.JWT_SECRET
};
