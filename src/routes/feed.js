const express = require("express");
const { body } = require("express-validator");

const feedController = require("../controllers/feed");
const auth = require("../middlewares/auth");
const router = express.Router();

router.get("/posts", feedController.getPosts);

router.get("/posts/:id", feedController.getPost);

router.post(
  "/posts",
  auth,
  [
    body("title")
      .trim()
      .isLength({ min: 5 }),
    body("content")
      .trim()
      .isLength({ min: 5 })
  ],
  feedController.createPost
);

router.put(
  "/posts/:id",
  auth,
  [
    body("title")
      .trim()
      .isLength({ min: 5 }),
    body("content")
      .trim()
      .isLength({ min: 5 })
  ],
  feedController.updatePost
);

router.delete("/posts/:id", auth, feedController.deletePost);

module.exports = router;
